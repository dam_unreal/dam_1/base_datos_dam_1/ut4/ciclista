#show tables;
#10
#select nombre, nomeq from ciclista;
#11
#select nombre from ciclista where nomeq='Banesto';

#16
SELECT DISTINCT c.nombre Ciclista
FROM ciclista c, maillot m, llevar l
WHERE c.dorsal=l.dorsal AND
l.codmaillot=m.codigo AND
m.tipo LIKE 'General';

#22
SELECT e.nomeq,e.director
FROM equipo e, ciclista c
WHERE c.nomeq=e.nomeq AND
c.edad>33;

#23
SELECT nombre Nombre,nomeq NEquipo
FROM ciclista
WHERE nomeq<>'Kelme';

#24
SELECT c.nombre 
FROM (ciclista c 
LEFT JOIN etapa e ON c.dorsal=e.dorsalganador) 
WHERE e.netapa IS NULL ;

#31
select p.nompuerto, p.categoria 
from ciclista c, puerto p 
where nomeq='Banesto' and c.dorsal=p.dorsalganador;
#32
select p.nompuerto as 'Puerto', p.netapa as 'Etapa', e.km as 'Km' 
from puerto p, etapa e 
where e.netapa=p.netapa;